import './App.css';
import Hover from './components/Hover';
import Click from './components/Click';

function App() {
  return (
    <div className="App">
      <Click name="Jignesh" contact="email"/>
      <Hover email="jg@gmail.com"/>
    </div>
  );
}

export default App;
