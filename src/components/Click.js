import { Component } from "react";
import hocCounter from "./HocCounter";

class Click extends Component {
    render() {
        const { count, incrCounter } = this.props;
        return <div>
            <div>
                Name:{this.props.name} <br />
            ContactVia: {this.props.contact}<br />
    Contact: {this.props.email} {/*This is not set for Click component so this will be empty*/}
            </div>
            <button onClick={incrCounter}>Click Counter: {count}</button>

            <hr />
        </div>
    }
}

export default hocCounter(Click);