import { Component } from "react";
const hocCounter = (OriginalComponent) => {
    class HocCounter extends Component {
        constructor() {
            super();
            this.state = {
                count: 0
            };
        }

        incrCounter = () => {
            this.setState((prevState) => {
                return { count: prevState.count + 1 }
            });
        }

        render() {
            return <OriginalComponent 
            count={this.state.count} 
            incrCounter={this.incrCounter}
            {...this.props} />
        }
    }
    return HocCounter;
}

export default hocCounter;