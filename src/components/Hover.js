import { Component } from "react";
import hocCounter from "./HocCounter";

class Hover extends Component {
    render() {
        const { count, incrCounter } = this.props;
        return <div>
            Contact: {this.props.email}<br />
            <span onMouseOver={incrCounter}>
                Hover Counter: {count}
            </span>
        </div>
    }
}

export default hocCounter(Hover);